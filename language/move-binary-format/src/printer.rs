use move_binary_format::file_format::*;
use std::{fs, env, result, error};

type Result<T> = result::Result<T, Box<dyn error::Error + 'static>>;

fn main() -> Result<()> {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        eprintln!("Usage: {} FILE_NAME", args[0]);
        return Ok(());
    }
    let bytes = fs::read(&args[args.len() - 1])?;
    if args[1].eq("-s") || args[1].eq("--script") {
        let script = CompiledScript::deserialize(&bytes)?;
        println!("{:?}", script);
    }
    else {
        let module = CompiledModule::deserialize(&bytes)?;
        println!("{:?}", module);
    }
    Ok(())
}
