use std::fmt;

pub struct PartialVMError;
pub type PartialVMResult<T> = ::std::result::Result<T, PartialVMError>;

impl PartialVMError {
    pub fn new() -> PartialVMError {
        PartialVMError {}
    }
}

impl fmt::Display for PartialVMError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "PartialVMError")
    }
}
