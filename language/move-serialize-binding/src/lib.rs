use std::{slice, ptr, alloc::{alloc, Layout}, collections::VecDeque, mem::size_of};

mod file_format;
mod values;
mod value;
mod errors;

/// Using self-defined types creates larger object than using dependencies
/// However, the library file is smaller
use crate::file_format::{SignatureToken, Constant};
use crate::values::{Value, Vector, AccountAddress};
// use move_binary_format::file_format::{SignatureToken, Constant};
// use move_vm_types::values::{Value, Vector};
// use move_core_types::account_address::AccountAddress;

// #[repr(C)]
// #[derive(Clone, Copy)]
// union IContainerVal {
//     value: *mut IValue,
//     u8_: *mut u8,
//     u64_: *mut u64,
//     u128_: *mut u128,
//     bool_: *mut bool,
//     address: *mut [u8; AccountAddress::LENGTH],
// }

// Ignoring ContainerRef and IndexedRef
// To make things easier, wrap all contained vals into IValue
// - This is an effect of unpack_hack()
#[derive(Clone, Copy)]
#[repr(C)]
pub struct IContainer {
    val: *mut IValue,
    val_size: usize,
}

#[derive(Clone, Copy)]
#[repr(C)]
#[repr(align(16))]
pub union IValueVal {
    u8_: u8,
    u64_: u64,
    u128_: u128,
    bool_: bool,
    address: [u8; AccountAddress::LENGTH],
    container: IContainer,
}

#[derive(Clone, Copy)]
#[repr(C)]
#[repr(align(16))]
pub struct IValue {
    ty: u8,
    val: IValueVal,
}

// Not supporting/allowing TypeParam, Ref, MutRef
#[derive(Clone, Copy)]
#[repr(C)]
pub struct ISignatureToken {
    ty: u8,
    token: *mut ISignatureToken, // Single elt for Vector, multiple for struct
    token_size: usize,
}

// pub unsafe fn value_from_interm(elt: IValue) -> Option<Value> {
//     match elt.ty {
//         1 => Some(Value::u8(elt.val.u8_)),
//         2 => Some(Value::u64(elt.val.u64_)),
//         3 => Some(Value::u128(elt.val.u128_)),
//         4 => Some(Value::bool(elt.val.bool_)),
//         5 => Some(Value::address(AccountAddress::new(elt.val.address))),
//         //NOTE signer (Vec addr), typeparam, ref, mutref not allowed
//         // struct, structinst not yet supported
//         // other Vec's supported
//         6 => match elt.val.container.ty {
//             3 => Some(Value::vector_u8(slice::from_raw_parts(elt.val.container.val.u8_,
//                                                             elt.val.container.val_size)
//                                       .to_vec())),
//             4 => Some(Value::vector_u64(slice::from_raw_parts(elt.val.container.val.u64_,
//                                                              elt.val.container.val_size)
//                                        .to_vec())),
//             5 => Some(Value::vector_u128(slice::from_raw_parts(elt.val.container.val.u128_,
//                                                               elt.val.container.val_size)
//                                         .to_vec())),
//             6 => Some(Value::vector_bool(slice::from_raw_parts(elt.val.container.val.bool_,
//                                                               elt.val.container.val_size)
//                                         .to_vec())),
//             7 => Some(Value::vector_address(slice::from_raw_parts(elt.val.container.val.address,
//                                                                  elt.val.container.val_size)
//                                            .iter()
//                                            .map(|&x| AccountAddress::new(x)))),
//             _ => None,
//         },
//         _ => None,
//     }
// }

// #[no_mangle]
// pub extern "C" fn serialize_value_size(value: IValue, token: ISignatureToken) -> usize {
//     let rust_token = if let Some(tok) = unsafe { signature_token_from_interm(token) } {
//         tok
//     }
//     else { return 0 };
    
//     let rust_value = if let Some(val) = unsafe { value_from_interm(value) } {
//         val
//     }
//     else { return 0 };
    
//     let type_layout = if let Some(lay) = Value::constant_sig_to_token_layout(&rust_token) {
//         lay
//     }
//     else { return 0 };
//     //TODO deserialize to bytes
// }

// macro_rules! error_to_null_ptr {
//     ($eval:expr) => {
//         match $eval {
//             Ok(a) => a,
//             Err(_) => return ptr::null_mut(),
//         }
//     }
// }

macro_rules! none_to_null_ptr {
    ($eval:expr) => {
        match $eval {
            Some(a) => a,
            None => return ptr::null_mut(),
        }
    }
}

macro_rules! error_to_none {
    ($eval:expr) => {
        match $eval {
            Ok(a) => a,
            Err(_) => return None,
        }
    }
}

macro_rules! pass_none {
    ($eval:expr) => {
        match $eval {
            Some(a) => a,
            None => return None,
        }
    }
}

//TODO use signature, cast to appropriate type, then construct
// for Vec, cast SignatureToken to Type, unpack, and recurse on Vec<Value>
// Return mut ptr instead
fn value_to_interm(value: Value, signature: ISignatureToken) -> Option<IValue> {
    let ivalue = match signature.ty {
        4 => {
            let addr: AccountAddress = error_to_none!(value.value_as());
            IValue { ty: 5, val: IValueVal { address: addr.into_bytes() } }
        },
        0 => {
            let num: bool = error_to_none!(value.value_as());
            IValue { ty: 4, val: IValueVal { bool_: num } }
        },
        1 => {
            let num: u8 = error_to_none!(value.value_as());
            IValue { ty: 1, val: IValueVal { u8_: num } }
        },
        2 => {
            let num: u64 = error_to_none!(value.value_as());
            IValue { ty: 2, val: IValueVal { u64_: num } }
        },
        3 => {
            let num: u128 = error_to_none!(value.value_as());
            IValue { ty: 3, val: IValueVal { u128_: num } }
        },
        6 => {
            // unpack hack, Vec<Value>, recurse 
            let vector: Vector = error_to_none!(value.value_as());
            let unpacked = error_to_none!(vector.unpack_hack());
            let len = unpacked.len();
            let layout = error_to_none!(Layout::array::<IValue>(len));
            let mut deque = VecDeque::from(unpacked);
            let container_ptr = unsafe {
                let ptr = alloc(layout) as *mut IValue;
                if ptr.is_null() {
                    return None;
                }
                for i in 0..len {
                    let v = pass_none!(deque.pop_front());
                    let iv = pass_none!(value_to_interm(v, *signature.token));
                    *ptr.offset(i as isize) = iv;
                }
                ptr
            };
            println!("Vec Len {}", len);
            IValue {
                ty: 6,
                val: IValueVal {
                    container: IContainer {
                        val: container_ptr,
                        val_size: len
                    }
                }
            }
        },
        //TODO struct
        _ => return None,
    };
    Some(ivalue)
}

//TODO Struct, StructInst
unsafe fn signature_token_from_interm(elt: ISignatureToken) -> Option<SignatureToken> {
    match elt.ty {
        0 => Some(SignatureToken::Bool),
        1 => Some(SignatureToken::U8),
        2 => Some(SignatureToken::U64),
        3 => Some(SignatureToken::U128),
        4 => Some(SignatureToken::Address),
        6 => {
            let token = if let Some(tok) = signature_token_from_interm(*elt.token) { tok }
            else { return None };
            Some(SignatureToken::Vector(Box::new(token)))
        },
        _ => None,
    }
}

#[no_mangle]
pub extern "C" fn deserialize_constant_binding(
    data: *const u8,
    data_size: usize,
    signature_token: ISignatureToken,
) -> *mut IValue {
    let token = none_to_null_ptr!(unsafe { signature_token_from_interm(signature_token) });

    let constant = Constant {
        type_: token,
        data: unsafe { slice::from_raw_parts(data, data_size) }.to_vec(),
    };

    let value = none_to_null_ptr!(Value::deserialize_constant(&constant));

    let ivalue = none_to_null_ptr!(value_to_interm(value, signature_token));

    match ivalue.ty {
        2 => println!("U64 type {}", unsafe { ivalue.val.u64_ }),
        _ => (),
    };
    
    let layout = Layout::new::<IValue>();
    println!("Sizeof IValue {}", size_of::<IValue>());
    println!("Sizeof IValueVal {}", size_of::<IValueVal>());
    println!("Sizeof IContainer {}", size_of::<IContainer>());
    unsafe {
        let ivalue_ptr = alloc(layout) as *mut IValue;
        if !ivalue_ptr.is_null() {
            *ivalue_ptr = ivalue;
        }
        ivalue_ptr
    }
}
