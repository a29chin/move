use serde::{
    Deserialize, Serialize,
};

#[derive(Clone, Serialize, Deserialize)]
pub enum MoveStructLayout {
    /// The representation used by the MoveVM
    Runtime(Vec<MoveTypeLayout>),
}

#[derive(Clone, Serialize, Deserialize)]
pub enum MoveTypeLayout {
    #[serde(rename(serialize = "bool", deserialize = "bool"))]
    Bool,
    #[serde(rename(serialize = "u8", deserialize = "u8"))]
    U8,
    #[serde(rename(serialize = "u64", deserialize = "u64"))]
    U64,
    #[serde(rename(serialize = "u128", deserialize = "u128"))]
    U128,
    #[serde(rename(serialize = "address", deserialize = "address"))]
    Address,
    #[serde(rename(serialize = "vector", deserialize = "vector"))]
    Vector(Box<MoveTypeLayout>),
    #[serde(rename(serialize = "struct", deserialize = "struct"))]
    Struct(MoveStructLayout),
    #[serde(rename(serialize = "signer", deserialize = "signer"))]
    Signer,
}

impl MoveStructLayout {
    pub fn fields(&self) -> &[MoveTypeLayout] {
        match self {
            Self::Runtime(vals) => vals,
        }
    }
}
